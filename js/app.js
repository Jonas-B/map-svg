var mapInteration = {
	// CONST
	WRAPPER_APP_LABEL: "#map-app",
	WRAPPER_MAP_LABEL: "#map",
	WRAPPER_MAP_REGION: ".land",
	WRAPPER_LIST_LABEL: "#list",

	// INIT
	init: function(){
		// COMPUTED CONST, MUST BE ID - CLASS
		this.WRAPPER_APP_LABEL = this.toId(this.WRAPPER_APP_LABEL)
		this.WRAPPER_MAP_LABEL = this.toId(this.WRAPPER_MAP_LABEL)
		this.WRAPPER_MAP_REGION = this.toClass(this.WRAPPER_MAP_REGION)
		this.WRAPPER_LIST_LABEL = this.toId(this.WRAPPER_LIST_LABEL)

		this.dataBind = {}
		try{
			this.cacheDOM()
			this.makeList()
		} catch(e){
			console.warn(e.msg)
		}
	},
	cacheDOM: function(){
		this.WRAPPER_APP = document.querySelector(this.WRAPPER_APP_LABEL)
		try{
			this.regions = this.WRAPPER_APP.querySelectorAll(this.WRAPPER_MAP_REGION)
		} catch (e){
			e.msg = "Can't initialize the module, the main div "+this.WRAPPER_APP_LABEL+" is not found"
			throw e
		}

		var wrap_tmp = this.WRAPPER_APP.querySelector(this.WRAPPER_LIST_LABEL)
		if(!wrap_tmp){
			wrap_tmp = document.createElement("div")
			wrap_tmp.classList.add("col-4")
			wrap_tmp.id = this.WRAPPER_LIST_LABEL.replace("#", "")
			wrap_tmp.appendChild(document.createElement("ul"))
			this.WRAPPER_APP.appendChild(wrap_tmp)
		}
		this.WRAPPER_LIST = wrap_tmp
	},

	// LIST GENERATION
	makeList: function(){
		this.regions.forEach(function(region){
			// LIST ELEMENT
			var li_tmp = document.createElement("li")
			li_tmp.id = region.id
			li_tmp.innerHTML = region.getAttributeNode("title").value
			this.WRAPPER_LIST.firstChild.appendChild(li_tmp)

			// DATABINDING
			this.dataBind[region.id] = {
				link: li_tmp,
				map: region
			}

			// INTERACTIONS
			li_tmp.addEventListener("mouseenter", this.handleEnter.bind(this))
			li_tmp.addEventListener("mouseout", this.handleLeave.bind(this))
			region.addEventListener("mouseenter", this.handleEnter.bind(this))
			region.addEventListener("mouseout", this.handleLeave.bind(this))
		}.bind(this))
	},

	// INTERACTIONS
	handleEnter: function(e){
		this.dataBind[e.target.id].link.classList.add("active")
		this.dataBind[e.target.id].map.classList.add("active")
	},
	handleLeave: function(e){
		this.dataBind[e.target.id].link.classList.remove("active")
		this.dataBind[e.target.id].map.classList.remove("active")
	},

	// UTILS
	toId: function(str){
		if(!str.indexOf("#")){
			if(str.indexOf(".")){
				str = str.replace(".", "#")
			}
			else{
				str = "#"+str
			}
		}
		return this.clean(str)
	},
	toClass: function(str){
		if(!str.indexOf(".")){
			if(str.indexOf("#")){
				str = str.replace("#", ".")
			}
			else{
				str = "."+str
			}
		}
		return this.clean(str)
	},
	clean: function(str){
		return str.replace(/\s/g,'_')
	}
}

mapInteration.init()